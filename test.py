import numpy as np
import random
import csv
import os
import pylab as pl
from pykalman import KalmanFilter


# reader = csv.reader(open("dataset/A"),delimiter=" ")
# reader.next()
# reader.next()
#
X = []

for file in sorted(os.listdir("dataset")):
    if file.startswith("X01"):
        reader = csv.reader(open(os.path.join("dataset/", file)), delimiter=" ")
        for row in reader:
            X.append(row[1::5])

X = np.array(X, dtype=np.float64)

np.savetxt("Results/test.csv", X, delimiter=',')
print(type(X))

# pl.figure(figsize=(8,6), dpi=150)
# # X06 = pl.plot(X[:,5], linestyle='-', color='b', label='OD6')
# # X07 = pl.plot(X[:,6], linestyle=':', color='g', label='OD7')
# # X08 = pl.plot(X[:,7], linestyle='-.', color='y', label='OD8')
# X06 = pl.plot(X[:,5])
# X07 = pl.plot(X[:,6])
# X08 = pl.plot(X[:,7])
#
# pl.title("OD")
# # pl.legend(['X06', 'X07', 'X08'])
# # pl.savefig("Results/test"+str(5)+".png")
# pl.show()


# test = np.array([[1,2,3,4],[4,5,6,7],[7,8,9,10]])
# test2 = test.transpose().reshape(3,4)
# print(np.linalg.norm(test-test2,axis=0)/np.linalg.norm(test,axis=0))