import numpy as np
import random
import csv
import os
import pylab as pl
from pykalman import KalmanFilter


reader = csv.reader(open("dataset/A"),delimiter=" ")
reader.next()
reader.next()

A = np.zeros((54,144))
X = []

for row in reader:
    A[int(row[2])-1,int(row[3])-1]=1

np.savetxt("dataset/A.csv", A, delimiter=',')
os.exit()

for file in sorted(os.listdir("dataset")):
    if file.startswith("X01"):
        reader = csv.reader(open(os.path.join("dataset/", file)), delimiter=" ")
        for row in reader:
            X.append(row[1::5])

X = np.array(X, dtype=np.float64)
#V0 = np.cov(X.transpose())
#x0 = np.mean(X.transpose(), axis=1)
Y = np.dot(A,np.transpose(X)).transpose()

kf = KalmanFilter(
    observation_matrices=A,
    #transition_matrices=np.eye((144)),
    #observation_covariance=np.eye((54)),
    #observation_covariance=np.eye((54))*1e-5,
    #observation_covariance=np.full((54,54),1e-5),
    #observation_covariance=np.zeros((54,54)),
    #transition_offsets=np.zeros((144)),
    #observation_offsets=np.zeros((54)),
    #initial_state_mean=np.zeros((144)),
    #initial_state_mean=np.full((144), 1),
    #initial_state_mean=x0,
    #initial_state_covariance=V0,
    em_vars=[
      'transition_offsets',
      'observationoffsets',
      'transition_matrices',
      'transition_covariance',
      'observation_covariance',
      'initial_state_mean',
      'initial_state_covariance'
      #'observation_covariance'

    ]
)


# for i in range(10):
    # kf=kf.em(np.transpose(Y), n_iter=1)#, em_vars=[
        #   'transition_matrices',
        #   'transition_covariance',
        #   'observation_covariance',
        #   #'initial_state_mean',
        #   'initial_state_covariance'
        # ])

kf=kf.em(Y, n_iter=2)
# kf=kf.em(Y)

    # np.set_printoptions(threshold=np.nan)
    # if (np.isnan(kf.transition_matrices).any() or np.isinf(kf.transition_matrices).any()):
    #     print('transition_matrices')
    #     print(kf.transition_covariance.shape)
    #     print(kf.transition_matrices)
    #     print(np.where(kf.transition_matrices == np.nan))
    #     print(np.where(kf.transition_matrices == np.inf))
    #     exit()
    # if (np.isnan(kf.transition_covariance).any() or np.isinf(kf.transition_covariance).any()):
    #     print('transition_covariance')
    #     print(kf.transition_covariance)
    #     exit()
    # if (np.isnan(kf.observation_covariance).any() or np.isinf(kf.observation_covariance).any()):
    #     print('observation_covariance')
    #     print(kf.observation_covariance)
    #     exit()
    # if (np.isnan(kf.initial_state_covariance).any() or np.isinf(kf.initial_state_covariance).any()):
    #     print('kf.initial_state_covariance')
    #     print(kf.initial_state_covariance)
    #     exit()


#np.savetxt("transition_matrix.csv", kf.transition_matrices, delimiter=",")
#np.savetxt("transition_covariance.csv", kf.transition_matrices, delimiter=",")
#exit()

filtered_state_estimates = kf.filter(Y)[0]
filtered_state_estimates_smooth = kf.smooth(Y)[0]

#for i in range (0,143):
for i in random.sample(range(0,143), 10):
    #pl.figure(figsize=(8,6), dpi=150)
    pl.figure()
    lines_true=pl.plot(X[:,i], linestyle='-', color='b')
    lines_filt = pl.plot(filtered_state_estimates[:,i], linestyle=':', color='g')
    lines_filt_smooth = pl.plot(filtered_state_estimates_smooth[:,i], linestyle=":", color='y')
    #pl.xlim(xmax=500)
    #pl.ylim(ymax=1000000, ymin=-1000000)
    pl.legend(['True', 'Filtered', 'Smooth'])
    pl.title("OD "+str(i+1))
    #pl.savefig("Results/OD"+str(i+1)+".png")

SRE = np.linalg.norm(filtered_state_estimates-X,axis=0)/np.linalg.norm(X,axis=0)
SRE_smooth = np.linalg.norm(filtered_state_estimates_smooth-X,axis=0)/np.linalg.norm(X,axis=0)
TRE = np.linalg.norm(filtered_state_estimates-X,axis=1)/np.linalg.norm(X,axis=1)
TRE_smooth = np.linalg.norm(filtered_state_estimates_smooth-X,axis=1)/np.linalg.norm(X,axis=1)

#pl.figure(figsize=(8,6), dpi=150)
pl.figure()
pl.plot(SRE, linestyle='-', color='b', label='SRE_Filter')
pl.plot(SRE_smooth, linestyle=':', color='g', label='SRE_Smooth')
pl.legend(['SRE_Filter', 'SRE_Smooth'])
pl.title('SRE')
#pl.savefig("Results/SRE.png")

#pl.figure(figsize=(8,6), dpi=150)
pl.figure()
pl.plot(TRE, linestyle='-', color='b', label='TRE_Filter')
pl.plot(TRE_smooth, linestyle=':', color='g', label='TRE_Smooth')
pl.legend(['TRE_Filter', 'TRE_Smooth'])
pl.title('TRE')
#pl.savefig("Results/TRE.png")

pl.show()

np.savetxt("Results/filtered_estimates.csv", filtered_state_estimates, delimiter=',')
np.savetxt("Results/filtered_smooth_estimates.csv", filtered_state_estimates_smooth, delimiter=',')
np.savetxt("Results/SRE.csv", SRE, delimiter=',')
np.savetxt("Results/SRE_Smooth.csv", SRE_smooth, delimiter=',')
np.savetxt("Results/TRE.csv", TRE, delimiter=',')
np.savetxt("Results/TRE_smooth.csv", TRE_smooth, delimiter=',')

print("finish")