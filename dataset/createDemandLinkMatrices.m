dmdIn = [];
dmdOut = [];
linkIn = [];
linkOut = [];
for node=0:11
    dmdOut = [dmdOut;sum(X(node*12+1:node*12+12,:))];
    dmdIn = [dmdIn; sum(X(node+1:12:144,:))];
end
linkIn = [linkIn;sum(Y(2,:),1)];
linkIn = [linkIn; sum(Y([1 11 14 29],:),1)];
linkIn = [linkIn; sum(Y([15 22],:),1)];
linkIn = [linkIn; sum(Y([17 27 24],:),1)];
linkIn = [linkIn; sum(Y([3 18 20],:),1)];
linkIn = [linkIn; sum(Y([4 19 6],:),1)];
linkIn = [linkIn; sum(Y([16 12 8],:),1)];
linkIn = [linkIn; sum(Y([13 25],:),1)];
linkIn = [linkIn; sum(Y([7 30],:),1)];
linkIn = [linkIn; sum(Y([21 9 28],:),1)];
linkIn = [linkIn; sum(Y([10 26],:),1)];
linkIn = [linkIn; sum(Y([5 23],:),1)];

linkOut = [linkOut; sum(Y(1,:),1)];
linkOut = [linkOut; sum(Y([2 5 4 3],:),1)];
linkOut = [linkOut; sum(Y([6 7],:),1)];
linkOut = [linkOut; sum(Y([8 9 10],:),1)];
linkOut = [linkOut; sum(Y([11 12 13],:),1)];
linkOut = [linkOut; sum(Y([14 15 16],:),1)];
linkOut = [linkOut; sum(Y([17 18 19],:),1)];
linkOut = [linkOut; sum(Y([20 21],:),1)];
linkOut = [linkOut; sum(Y([22 23],:),1)];
linkOut = [linkOut; sum(Y([24 25 26],:),1)];
linkOut = [linkOut; sum(Y([27 28],:),1)];
linkOut = [linkOut; sum(Y([29 30],:),1)];

%%
for i = 1:12
    subplot(3,4,i);
    plot(dmdOut(i,:));
    hold;
    plot(linkOut(i,:),':');
    plot(abs(linkOut(i,:)-dmdOut(i,:)));
    title(['Node ', num2str(i)]);
    legend('dmdOut', 'linkOut', 'diff');
    xlim([0, 2015]);
end

%%
for i = 1:12
    subplot(3,4,i);
    plot(dmdIn(i,:));
    hold;
    plot(linkIn(i,:),':');
    plot(abs(linkIn(i,:)-dmdIn(i,:)));
    title(['Node ', num2str(i)]);
    legend('dmdIn', 'linkIn', 'diff');
    xlim([0,2015]);
end